import styled from "styled-components";
import { View, Text } from "react-native";

export const Wrapper = styled(View)`
  flex-direction: row;
  align-items: center;
`;

export const Container = styled(View)`
  flex-direction: row;
  align-items: center;
  padding: 15px;
  border: 1px solid #ccc;
  margin-bottom: 15px;
  border-radius: 5px;
  justify-content: space-between;
`;

export const Title = styled(Text)`
  margin-left: 10px;
`;

export default { Container };
