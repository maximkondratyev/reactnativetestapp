import React from "react";
import { inject } from "mobx-react";
import { Button } from "react-native";

const HeaderRight = ({ TasksStore }) => {
  const { doneTasks } = TasksStore;

  const handleFilterTasks = () => {
    console.log(doneTasks());
  };

  return <Button onPress={handleFilterTasks} title="Filter" />;
};

export default inject("TasksStore")(HeaderRight);
