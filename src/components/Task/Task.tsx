import React from "react";
import { observer } from "mobx-react";
import { Instance } from "mobx-state-tree";
import { Button } from "react-native";
import CheckBox from "@react-native-community/checkbox";
import { Container, Wrapper, Title } from "./Task.styles";
import { Task as TaskModel } from "../../store/TasksStore";

type TaskProps = {
  task: Instance<typeof TaskModel>;
  removeTask: (task: Instance<typeof TaskModel>) => void;
};

const Task = ({ task, removeTask }: TaskProps) => {
  const handleRemove = () => {
    removeTask(task);
  };

  const setDone = () => {
    task.toggleDone();
  };

  return (
    <Container>
      <Wrapper>
        <CheckBox value={task.done} onChange={setDone} />
        <Title>{task.title}</Title>
      </Wrapper>
      <Button title="X" onPress={handleRemove} />
    </Container>
  );
};

export default observer(Task);
