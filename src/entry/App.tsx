import React from "react";
import { Provider } from "mobx-react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HeaderRight from "../components/HeaderRight";
import Tasks from "../screens/Tasks/Tasks";
import AddTask from "../screens/AddTask/AddTask";
import TasksStore from "../store/TasksStore";

const Stack = createStackNavigator();

const stores = {
  TasksStore,
};

const App = () => {
  return (
    <Provider {...stores}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Tasks">
          <Stack.Screen
            name="Tasks"
            component={Tasks}
            options={{
              headerRight: () => <HeaderRight />,
            }}
          />
          <Stack.Screen name="AddTask" component={AddTask} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
