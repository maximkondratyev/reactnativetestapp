import styled from "styled-components";
import { View, TextInput, Button } from "react-native";

export const Container = styled(View)`
  flex: 1;
  justify-content: center;
  padding: 20px;
`;

export const Input = styled(TextInput)`
  width: 100%;
  padding: 10px;
  border: 1px solid #ccc;
  margin-bottom: 15px;
  text-align: center;
  border-radius: 5px;
`;

export const Submit = styled(Button)`
  margin: 0 20px;
`;

export default { Container, Input, Submit };
