import React, { useState } from "react";
import { inject } from "mobx-react";
import { Container, Input, Submit } from "./AddTask.styles";
import { ToastAndroid } from "react-native";

const AddTask = ({ TasksStore, navigation }) => {
  const { addTask } = TasksStore;

  const [value, setValue] = useState<string>("");

  const handleSubmit = () => {
    if (value) {
      addTask({
        id: Date.now().toString(),
        title: value,
        done: false,
      });
      navigation.navigate("Tasks");
    } else {
      ToastAndroid.show("Task name must not be empty", ToastAndroid.SHORT);
    }
  };

  return (
    <Container>
      <Input onChangeText={setValue} value={value} />
      <Submit title="Save" onPress={handleSubmit} />
    </Container>
  );
};

export default inject("TasksStore")(AddTask);
