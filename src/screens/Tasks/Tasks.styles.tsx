import styled from "styled-components";
import { View } from "react-native";

export const Container = styled(View)`
  padding: 20px;
`;

export default { Container };
