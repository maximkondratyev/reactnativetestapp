import React from "react";
import { inject, observer } from "mobx-react";
import { Button, FlatList } from "react-native";
import Task from "../../components/Task/Task";
import { Container } from "./Tasks.styles";

const Tasks = ({ TasksStore, navigation }) => {
  const { tasks, removeTask } = TasksStore;

  const handleAddTask = () => {
    navigation.navigate("AddTask");
  };

  return (
    <Container>
      <FlatList
        data={[...tasks]}
        keyExtractor={item => item.id.toString()}
        renderItem={({ item }) => <Task task={item} removeTask={removeTask} />}
      />
      <Button title="Add task" onPress={handleAddTask} />
    </Container>
  );
};

export default inject("TasksStore")(observer(Tasks));
