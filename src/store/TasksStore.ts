import { types, destroy, Instance } from "mobx-state-tree";

export const Task = types
  .model("Task", {
    id: types.string,
    title: types.string,
    done: types.boolean,
  })
  .actions(self => ({
    toggleDone() {
      self.done = !self.done;
    },
  }));

const initialState = [
  {
    id: "1",
    title: "Learn TypeScript",
    done: false,
  },
];

const TasksStore = types
  .model("Tasks", {
    tasks: types.array(Task),
  })
  .actions(self => ({
    addTask(task: Instance<typeof Task>) {
      self.tasks.push(task);
    },
    removeTask(task: Instance<typeof Task>) {
      destroy(task);
    },
  }))
  .views(self => ({
    doneTasks() {
      return self.tasks.filter(task => task.done);
    },
  }))
  .create({
    tasks: initialState,
  });

export default TasksStore;
